import React, { Component } from "react";
import { connect } from "react-redux";

class Redux_Mini extends Component {
  render() {
    console.log(this.props);
    return (
      <div>
        <button
        onClick={()=>this.props.handleChangeNumber('GIAM_SO_LUONG', 5)}
        className="btn btn-danger">-</button>
        <strong className="mx-3">{this.props.num}</strong>
        <button
          onClick={()=>this.props.handleChangeNumber('TANG_SO_LUONG', 5)}
          className="btn btn-success"
        >
          +
        </button>
      </div>
    );
  }
}

let mapPropsToState = (state) => {
  return {
    num: state.miniReducer.number,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleChangeNumber: (hanhDong,soLuong) => {
      let action = {
        type: hanhDong,
        payload: soLuong
      };
      dispatch(action);
    },
  };
};

export default connect(mapPropsToState, mapDispatchToProps)(Redux_Mini);
