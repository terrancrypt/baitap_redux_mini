let initialValue = {
    number: 1,
}

export const miniReducer = (state = initialValue, action) =>{
    switch(action.type){
        case "TANG_SO_LUONG":{
            state.number += action.payload;
            return{...state}
        }case "GIAM_SO_LUONG":{
            state.number -= action.payload;
            return{...state}
        }
        default:{
            return state;
        }
    }
}