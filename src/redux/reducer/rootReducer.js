import { combineReducers } from "redux";
import { miniReducer } from "./miniReducer";

export const rootReducer = combineReducers({
    miniReducer,
})