import './App.css';
import Redux_Mini from './Redux_Mini/Redux_Mini';

function App() {
  return (
    <div className="container py-5">
      <Redux_Mini/>
    </div>
  );
}

export default App;
